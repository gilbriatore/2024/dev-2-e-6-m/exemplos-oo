package br.edu.up.modelos;

public class Carro extends Veiculo {

    // public static enum Tipo {
    // UNO,
    // BMW
    // }

    // Atributos
    // private Tipo tipo;
    private String marca;
    private String modelo;
    private int velocidadeMaxima;
    private int velocidade = 0;

    public Carro(int velocidadeMaxima) {
        this.velocidadeMaxima = velocidadeMaxima;
    }

    // Getter (pegar)
    public String getMarca() {
        return this.marca;
    }

    public String getModelo() {
        return this.modelo;
    }

    public int getVelocidade() {
        return this.velocidade;
    }

    public int getVelocidadeMaxima() {
        return this.velocidadeMaxima;
    }

    // Setter (atribuir)
    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setVelocidadeMaxima(int velocidadeMaxima) {
        this.velocidadeMaxima = velocidadeMaxima;
    }

    public void acelerar() {
        if (this.velocidade < this.velocidadeMaxima) {
            this.velocidade++;
        }

        // if (tipo.equals(Tipo.UNO) && this.velocidade < 144) {
        // this.velocidade++;
        // } else if (tipo.equals(Tipo.BMW) && this.velocidade < 240) {
        // this.velocidade++;
        // }
    }

    public void frear() {
        if (this.velocidade > 0) {
            this.velocidade--;
        }
    }
}
